Lenovo TB-8704X Phone is An example for MTK Phone, and we use it.

in this article, I will guide you on how to root and install Twrp on Lenovo TB-8704X after bootloader unlock. 

There are various reasons to root Lenovo TB-8704X.

For example, you want to remove the FRP lock from it. Installation of custom ROMs and removing pre-installed OEM apps.

Pre-requirements To Root Lenovo TB-8704X
1. Before going to ‘Root Lenovo TB-8704X’ you need to take all your messages, photos, and apps from your current smartphone.

Most smartphones automatically make a backup in the background, in case something happens. But to make sure you take everything to your device, you need a full backup.

How To Backup Android Phone Before Root

2. Download Universal ADB Drivers here (If your PC have already drivers installed then skip this)

3. Twrp 3.2.1 Lenovo TB-8704X And vbmeta.img

4. Download SuperSU by Chainfire from here.

5. Download Magisk Manager from Magisk 19.3.

Unlock Bootloader of Lenovo TB-8704X https://aiomobilestuff.com/

1. Connect your Lenovo TB-8704X Device to the computer, in Fastboot Mode. You can reboot your device to fastboot using the command in ADB terminal:

adb reboot bootloader

2. Now, open the Adb Terminal in PC, and then just enter this command:

fastboot oem unlock-go
Or
fastboot oem unlock

3. your device will be reboot automatically. Now, you can Root Lenovo TB-8704X, as the bootloader of your device is already unlocked.

How To Install Twrp on Lenovo TB-8704X
1. Enable the developer settings on your Lenovo TB-8704X Device.

2. Navigate to the Settings -> About and then find the ‘Build Number ‘, and then tap on ‘Build Number’ for 7 times, this will enable the developer settings on your device.

3. Open the command prompt in computer/ ADB Terminal, and simply type the command:

adb reboot bootloader
4. Now copy the Twrp.img file downloaded in prerequisite section, to the same folder where ADB and fastboot binaries are located. Rename the file to ‘Twrp.img’ and then enter this command:

fastboot flash recovery recovery.img
fastboot flash vbmeta vbmeta.img
fastboot boot recovery.img
Then

fastboot reboot
5. And reboot your device.

How To Root Lenovo TB-8704X [Magisk-Supersu]
Via Supersu
1. To root Lenovo TB-8704X Via Supersu download & Copy the SuperSU.Zip files on your internal storage or SD card.

2. Restart your Lenovo TB-8704X into the recovery.

Turn off your device.

Press & hold the Volume Down + power button, at the same time for about 5 seconds.

3. Click Install and scroll down and choose Update superSU and swipe to confirm flash.

Via Supersu

 4. Once the installation is finished, click on Reboot System to reboot your phone.

5. That’s it ! you have successfully ‘Root Lenovo TB-8704X via SuperSU’

Via Magisk
1. To root Lenovo TB-8704X via Magisk Copy the Magisk.zip file on your internal storage or SD card.

2. Restart your Lenovo TB-8704X into the recovery.

3. Turn off your device.

4. Press & hold the Volume Down + power button, at the same time for about 5 seconds.

5. Click Install and scroll down and choose Magisk.zip and swipe to confirm flash.

6. After install complete press the return key. Select reboot to restart the phone in normal mode.

7. install the apk file “MagiskManager.apk”

8. Now you have successfully Root Lenovo TB-8704X via Magisk.

Download the Root Checker app that allows you to check in seconds if your mobile is rooted or not. If you want to unroot open SuperSu app click on Full Unroot.

unroot

Conclusion:

Did you read and understand the Full Guide Before root Lenovo TB-8704X?
Are you willing to risk facing and trying to fix the disadvantages of rooting your device?
Have you made a backup for your device before it is attempted to be rooted?
If your answer to these three questions is a ‘yes’, then you should be ready to handle rooting your device and facing the consequences and benefits.

If your answer is ‘no’ to all three questions, then you should probably think a few more times before you take the leap. Or leave out rooting your device altogether.
https://aiomobilestuff.com/